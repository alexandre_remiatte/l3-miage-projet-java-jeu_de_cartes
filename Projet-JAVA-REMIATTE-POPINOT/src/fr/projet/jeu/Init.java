package fr.projet.jeu;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import fr.projet.elements.Carte_Cardline;
import fr.projet.enums.MODE_DE_JEU;
import fr.projet.enums.TYPE_DE_PARTIE;


@SuppressWarnings("serial")
public class Init implements Serializable {
	

	private JFrame frame;
	private JPanel contentPane;

	private Jeu_de_cartes le_jeu;
	
	// Chemins de fichiers
	private final String chemin_image_fond = ".\\DATA\\images\\fond.jpg";
	private final String chemin_image_parchemin = ".\\DATA\\images\\Parchemin.png";
	private final String chemin_image_infojeu = ".\\DATA\\images\\InfoJeu.png";
	private final String chemin_image_infoaction = ".\\DATA\\images\\infoaction.png";
	private final String chemin_image_logo_timeline = ".\\DATA\\images\\timeline.png";
	private final String chemin_image_logo_cardline = ".\\DATA\\images\\cardline.png";
	
	/*
	 * MAIN
	 * Start class Init
	 */
	public static void main(String[] args) {
		new Init();
	}
	
	
	/*
	 * Constructeur
	 */
	public Init() {
		startFrame(); //Lance la fen�tre de jeu (vide pour le moment)
		menu_jeu();
	}
	
	/*
	 * On demande si l'utilisateur veut cr�er ou charger une partie
	 */
	protected void menu_jeu() {
		TYPE_DE_PARTIE result = (TYPE_DE_PARTIE) JOptionPane.showInputDialog( //On demande si l'utilisateur veut cr�er ou charger une partie
        frame,
        "Voulez-vous cr�er une partie ? ou charger une partie ?",
        "MENU DU JEU DE CARTES",
        JOptionPane.PLAIN_MESSAGE,
        new ImageIcon("C:\\Users\\PC-FIX Alex\\OneDrive - Universite de Lorraine\\COURS\\L3 MIAGE\\JAVA\\Projet\\workspace2\\Projet 0.4\\src\\data\\icon\\windows-question.png"),
        TYPE_DE_PARTIE.values(),
        "");
		start_game(result); //On lance la partie en fonction du choix
	}
	
	
	/*
	 * M�thode qui lance une partie en fonction de : Cr�ation partie ou Chargement partie
	 */
	private void start_game(TYPE_DE_PARTIE le_type_partie) {
		if (le_type_partie == TYPE_DE_PARTIE.Nouvelle_partie) 
			this.le_jeu = new Jeu_de_cartes(this);						//On cr�er une partie, un objet de la classe Jeu_de_Cartes
		else if (le_type_partie == TYPE_DE_PARTIE.Charger_une_partie)
			load_game();												//On charge une partie
	}
	
	
	/*
	 * M�thode update()
	 * Elle permet de (re)g�n�rer l'affichage sur la fen�tre du jeu
	 * Elle recharge tout le contenu des JPanel ou m�me la JFrame
	 */
	public void update() {
		
		update_contentPane(); // mise � jour JPanel et Image de fond
		
		update_affichage_des_messages(); // mise � jour des informations de la partie, des images

		update_zone_centrale_plateau_de_jeu();// mise � jour des cartes du plateau de jeu

		update_zone_centrale_main_joueur(); // mise � jour des cartes du joueur actif
		
		refresh(); // On refresh pour rendre visible les modification effectu�s
	}
	
	/*
	 * Mise � jour JPanel et Image de fond
	 */
	private void update_contentPane() {
		contentPane = null; // Reset
		
		//cr�ation du JPanel et de l'image de fond
		contentPane = new JPanel();
		contentPane.setOpaque(false);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		frame.setContentPane(contentPane);
		contentPane.setLayout(null);
		JLabel contentPane2 = new JLabel();
		contentPane2.setIcon( new ImageIcon(this.chemin_image_fond));
		contentPane2.setLayout( new BorderLayout() );
		frame.setContentPane(contentPane2);
		frame.getContentPane().add(contentPane);
	}

	/*
	 * Mise � jour des informations de la partie et des images
	 */
	private void update_affichage_des_messages() {
		
		// Logo du jeu (TimeLine ou CardLine)
		String chemin_logo;
		if (this.le_jeu.getLe_mode_de_jeu() == MODE_DE_JEU.Cardline)
			chemin_logo = chemin_image_logo_cardline;
		else
			chemin_logo = chemin_image_logo_timeline;
		JLabel logojeu = new JLabel(new ImageIcon(new ImageIcon(chemin_logo).getImage().getScaledInstance(150, 50, Image.SCALE_DEFAULT)));
		logojeu.setBounds(100, 10, 150, 50);
		contentPane.add(logojeu);
		
		// Affichage du crit�re du pays, si le mode de jeu est CardLine
		if (this.le_jeu.getLe_mode_de_jeu() == MODE_DE_JEU.Cardline) {
			JLabel affiche_critere_cardline = new JLabel("Crit�re : " + Carte_Cardline.getValeur_active());
			affiche_critere_cardline.setBounds(1165, 120, 200, 14);
			contentPane.add(affiche_critere_cardline);
		}
		
		//Affichage de la liste des joueurs ainsi que le nombre de leurs cartes
		JLabel parchemin = new JLabel(new ImageIcon(new ImageIcon(chemin_image_parchemin).getImage().getScaledInstance(250, 300, Image.SCALE_DEFAULT)));
		parchemin.setBounds(1100, 200, 250, 300);
		JLabel titre_liste_joueur = new JLabel("Liste des joueurs :");
		titre_liste_joueur.setBounds(1160, 120, 250, 300);
		contentPane.add(titre_liste_joueur);
		for (int i=0; i<this.le_jeu.getListe_des_joueurs().size();i++) {
			JLabel liste_joueur_i = new JLabel("  - "+this.le_jeu.getListe_des_joueurs().get(i).getPseudo() + "["+this.le_jeu.nb_cartes_restantes_joueur(i)+"]");
			liste_joueur_i.setBounds(1165, 290+(i*15), 150, 14);
			if (this.le_jeu.getId_joueur_actif() == i)
				liste_joueur_i.setForeground(Color.RED);
			contentPane.add(liste_joueur_i);
		}
		contentPane.add(parchemin);
		
		// Affichage de l'Info jeu
		JLabel infojeu = new JLabel(new ImageIcon(new ImageIcon(chemin_image_infojeu).getImage().getScaledInstance(250, 150, Image.SCALE_DEFAULT)));
		infojeu.setBounds(1100, 20, 250, 150);
		JLabel titre_joueur = new JLabel("C'est au tour de :");
		titre_joueur.setBounds(1170, 75, 120, 14);
		contentPane.add(titre_joueur);
		JLabel nom_joueur = new JLabel(this.le_jeu.getNom_joueur_actif()+" !");
		nom_joueur.setBounds(1180, 90, 120, 14);
		contentPane.add(nom_joueur);
		contentPane.add(infojeu);
		
		// Affichage de la prochaine Action
		JLabel infoaction = new JLabel(new ImageIcon(new ImageIcon(chemin_image_infoaction).getImage().getScaledInstance(550, 70, Image.SCALE_DEFAULT)));
		infoaction.setBounds(500, 1, 550, 70);
		JLabel titre_prochaine_action = new JLabel("Prochaine action : " +this.le_jeu.getProchaineAction());
		titre_prochaine_action.setBounds(550, 25, 550, 14);
		contentPane.add(titre_prochaine_action);
		contentPane.add(infoaction);
		
	}

	/*
	 * Mise � jour de l'affichage du plateau de jeu
	 */
	private void update_zone_centrale_plateau_de_jeu() {
		
		// JPanel
		JPanel zone_centrale = new JPanel();
		zone_centrale.setBounds(0, 0, 1066, 290);
		zone_centrale.setBackground(Color.lightGray);
		
		// premier bouton +
		JButton boutton1 = new JButton("+");
		boutton1.addActionListener(new button_zone_centrale(this,0));
		boutton1.setBounds(0, 105, 40, 40);
		zone_centrale.add(boutton1);
		
		// Boucle sur les cartes et affichage d'une carte suivi d'un bouton +
		for (int i=0;i<this.le_jeu.getDeck_central_frise().getNb_Cartes();i++) {
			ImageIcon icon = new ImageIcon(new ImageIcon(this.le_jeu.getDeck_central_frise().getCartes().get(i).getChemin_image_reponse()).getImage().getScaledInstance(175, 262, Image.SCALE_DEFAULT));
			JLabel img = new JLabel(icon);
			img.setBounds(i*175+10, 0, 175, 262);
			zone_centrale.add(img);

			JButton boutton_i = new JButton("+");
			boutton_i.addActionListener(new button_zone_centrale(this,i+1));
			boutton_i.setBounds(i*175+10, 105, 40, 40);
			zone_centrale.add(boutton_i);
		}
		
		// Cr�ation de la scrollPane final (du plateau)
		JScrollPane scrollPanex = new JScrollPane();
		scrollPanex.setViewportView(zone_centrale);
		scrollPanex.setBounds(0, 70, 1066, 290);
		contentPane.add(scrollPanex);
	}

	/*
	 *  mise � jour des cartes du joueur actif
	 */
	private void update_zone_centrale_main_joueur() {
		JPanel zone_main_joueur = new JPanel();
		zone_main_joueur.setBounds(0, 0, 1066, 290);
		//Pour chaque carte de la main du joueur actif
		for (int i=0;i<this.le_jeu.getListe_des_joueurs().get(this.le_jeu.getId_joueur_actif()).getDeck_joueur().getNb_Cartes();i++) {
			ImageIcon icon2 = new ImageIcon(new ImageIcon(this.le_jeu.getListe_des_joueurs().get(this.le_jeu.getId_joueur_actif()).getDeck_joueur().getCartes().get(i).getChemin_image()).getImage().getScaledInstance(175, 262, Image.SCALE_DEFAULT));
			//V�rification de la carte (Si c'est celle qui est s�lectionn�e)
			if (this.le_jeu.getCarte_selectionnee() == this.le_jeu.getListe_des_joueurs().get(this.le_jeu.getId_joueur_actif()).getDeck_joueur().getCartes().get(i)) {
				//OUI, alors on cr�er la carte avec une bordure !
				int borderWidth = 1;
				int spaceAroundIcon = 0;
				Color borderColor = Color.RED;

				BufferedImage bi = new BufferedImage(icon2.getIconWidth() + (2 * borderWidth + 2 * spaceAroundIcon),icon2.getIconHeight() + (2 * borderWidth + 2 * spaceAroundIcon), BufferedImage.TYPE_INT_ARGB);

				Graphics2D g = bi.createGraphics();
				g.setColor(borderColor);
				g.drawImage(icon2.getImage(), borderWidth + spaceAroundIcon, borderWidth + spaceAroundIcon, null);

				BasicStroke stroke = new BasicStroke(5); //5 pixels wide (thickness of the border)
				g.setStroke(stroke);

				g.drawRect(0, 0, bi.getWidth() - 1, bi.getHeight() - 1);
				g.dispose();

				JLabel label = new JLabel(new ImageIcon(bi), JLabel.LEFT);
				label.setVerticalAlignment(SwingConstants.TOP); 
				label.setBounds(i*175+10, 0, 175, 262);
				zone_main_joueur.add(label);
			} else {
				//Non, on affiche la carte sans bordure
				JLabel img2 = new JLabel(icon2);
				img2.addMouseListener(new MouseAdapter_image(this.le_jeu.getListe_des_joueurs().get(this.le_jeu.getId_joueur_actif()).getDeck_joueur().getCartes().get(i),this));
				img2.setBounds(i*175+10, 0, 175, 262);
				zone_main_joueur.add(img2);
			}
		}
		
		//Cr�ation du scrollPane final de la main du joueur
		JScrollPane scrollPane_main_joueur = new JScrollPane();
		scrollPane_main_joueur.setViewportView(zone_main_joueur);
		scrollPane_main_joueur.setBounds(0, 360, 1066, 290);
		contentPane.add(scrollPane_main_joueur);
	}

	/*
	 * Mise � jour de l'affichage de la JFrame
	 */
	private void refresh() {
		frame.setVisible(true);
	}


	/*
	 * Lance la fen�tre de jeu
	 */
	private void startFrame() {
		init_frame(); // Cr�ation et configuration des param�tres
		add_menu(); // Ajout du menu � la JFrame
		frame.setVisible(true); // affichage JFrame
	}
	
	/*
	 * Cr�ation et configuration des param�tres de la JFrame
	 */
	private void init_frame() {
		frame = new JFrame("[JEUX DE CARTES] Timeline/Cardline");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setBounds(100, 100, 1366, 700);
		frame.setResizable(false);
		contentPane = new JPanel();
		contentPane.setOpaque(false);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		frame.setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// affichage temporaire de l'image de fond (En attendant la configuration de la partie
		JLabel contentPane2 = new JLabel();
		contentPane2.setIcon( new ImageIcon(this.chemin_image_fond));
		contentPane2.setLayout( new BorderLayout() );
		frame.setContentPane(contentPane2);
 
		frame.getContentPane().add(contentPane);
	}
	

	/*
	 * Configure et ajout le menu � la JFrame
	 */
	private void add_menu() {
		//MENU bar
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		// MENU jeu
		JMenu mnJeu = new JMenu("Jeu");
		JMenuItem menu_new_game = new JMenuItem("Nouvelle partie");
		menu_new_game.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (msg_confirmation_arret()) {
					close_game();
					start_game(TYPE_DE_PARTIE.Charger_une_partie);
				}
			}
		});
		mnJeu.add(menu_new_game);
		JMenuItem menu_load_game = new JMenuItem("Charger une partie");
		menu_load_game.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (msg_confirmation_arret()) 
					load_game();
			}
		});
		mnJeu.add(menu_load_game);
		JMenuItem menu_save_game = new JMenuItem("Sauvgarder la partie");
		menu_save_game.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				save_game();
			}
		});
		mnJeu.add(menu_save_game);
		menuBar.add(mnJeu);

		//MENU Param�tres partie
		JMenu mnPartie = new JMenu("Param�tres partie");
		JMenuItem menuItem_changer_de_mode_de_jeu = new JMenuItem("Changer le mode de jeu");
		menuItem_changer_de_mode_de_jeu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				le_jeu.charger_critere_cardline(true);
			}
		});
		mnPartie.add(menuItem_changer_de_mode_de_jeu);
		menuBar.add(mnPartie);
		
		//Menu Quitter
		JMenu mnQuitter = new JMenu("Quitter");
		JMenuItem menuItem_quitter_le_jeu = new JMenuItem("Quitter sans sauvgarder ! ");
		menuItem_quitter_le_jeu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (msg_confirmation_arret())
					quitter_le_jeu();
			}
		});
		mnQuitter.add(menuItem_quitter_le_jeu);
		JMenuItem menu_quitter_et_sauv = new JMenuItem("Sauvgarder et quitter");
		menu_quitter_et_sauv.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				save_game();
				quitter_le_jeu();
			}
		});
		mnQuitter.add(menu_quitter_et_sauv);
		menuBar.add(mnQuitter);
	}

	/*
	 * Ferme la fen�tre de jeu
	 */
	void quitter_le_jeu() {
		frame.dispose();
	}


	/*
	 * Arrete la partie 
	 */
	public void close_game() {
		this.le_jeu=null;
		this.contentPane.setVisible(false);
	}
	
	
	/*
	 * Chargement d'une partie sauvegard�e
	 */
	private void load_game() {
		// On demande le lieu du fichier � l'utilisateur
		final JFileChooser fc = new JFileChooser();
		fc.setDialogTitle("S�lectionner votre fichier de sauvegarde");
		fc.showOpenDialog(fc);
		// On charge la sauvegarde dans un objet Jeu_de_cartes
		String url = fc.getSelectedFile().getAbsolutePath();
		ObjectInputStream ois = null;
	    try {
	      final FileInputStream fichier = new FileInputStream(url);
	      ois = new ObjectInputStream(fichier);
	      Jeu_de_cartes le_jeu = null;
	      le_jeu = (Jeu_de_cartes) ois.readObject(); // On cr�� l'objet
	      this.le_jeu = le_jeu;						//On d�fini ce jeu comme jeu actif
	      this.le_jeu.setInit_frame(this);			//On d�fini le Init_frame comme this (pour que le_jeu puise faire un update)
	      this.le_jeu.update_reload_game();			//On recharge le jeu (Mise ne m�moire du crit�re de pays, si besoin)
	    } catch (final java.io.IOException e) {
	      e.printStackTrace();
	    } catch (final ClassNotFoundException e) {
	      e.printStackTrace();
	    } finally {
	      try {
	        if (ois != null) {
	          ois.close();
	        }
	      } catch (final IOException ex) {
	        ex.printStackTrace();
	      }
	    }
	    this.update();
	    JOptionPane.showMessageDialog(
				null,
				"Votre partie a �t� charg�e !",
				"Chargement partie", 
				JOptionPane.INFORMATION_MESSAGE);
	}
	
	/*
	 * Sauvegarde d'une partie
	 */
	private void save_game() {
		// On demande � l'utilisateur le chemin du fichier
		final JFileChooser fc = new JFileChooser();
		fc.setDialogTitle("O� voulez-vous sauvegarder votre partie ?");
		fc.showOpenDialog(fc);
		//On cr�er la sauvegarde
		String url = fc.getSelectedFile().getAbsolutePath();
		ObjectOutputStream oos = null;
	    try {
	      FileOutputStream fichier = new FileOutputStream(url);
	      oos = new ObjectOutputStream(fichier);
	      oos.writeObject(this.le_jeu); //En utilisant le_jeu comme objet global
	      oos.flush();
	    } catch (final java.io.IOException e) {
	      e.printStackTrace();
	    } finally {
	      try {
	        if (oos != null) {
	          oos.flush();
	          oos.close();
	        }
	      } catch (final IOException ex) {
	        ex.printStackTrace();
	      }
	    }
	    JOptionPane.showMessageDialog(
		null,
		"Votre partie a �t� sauvegard�e !",
		"SAUVEGARDE", 
		JOptionPane.INFORMATION_MESSAGE);
	}
	
	
	/*
	 * Message de confirmation d'arret de la partie
	 */
	private boolean msg_confirmation_arret() {
		return JOptionPane.showConfirmDialog(null, "Attention cette action va supprimer la partie en cours. \n"
				+ "Merci de sauvegarder votre partie avant ! \n"
				+ "�tes-vous sur de continuer ?", "Confirmation",
		        JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION;
	}
	
	
	/*
	 * Get le_jeu
	 */
	public Jeu_de_cartes getLe_jeu() {
		return le_jeu;
	}

	
}
