package fr.projet.jeu;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import fr.projet.elements.Carte;
import fr.projet.elements.Carte_Cardline;
import fr.projet.elements.Chargeur_cartes;
import fr.projet.elements.Deck;
import fr.projet.elements.Joueur;
import fr.projet.enums.CRITERE_CARDLINE;
import fr.projet.enums.MODE_DE_JEU;

@SuppressWarnings("serial")
public class Jeu_de_cartes implements Serializable {

	
	private JFrame frame;
	
	private Deck deck_du_jeu;
	private Deck deck_central_frise = new Deck();
	private Deck deck_cartes_supprimee = new Deck();
	private ArrayList<Joueur> liste_des_joueurs;
	private int id_joueur_actif;
	private CRITERE_CARDLINE save_critereCardline;
	private Carte carte_selectionnee;
	private MODE_DE_JEU le_mode_de_jeu;
	private Init init_frame;
	
	/*
	 * Constructeur
	 */
	public Jeu_de_cartes(Init init_frame) {
		this.id_joueur_actif = 0; 			// On d�fini le premier joueur
		this.init_frame = init_frame; 		// On d�fni la Frame qui se charge de l'affichage
		@SuppressWarnings("unused")
		Init_jeu init = new Init_jeu(this);	// On lance la fen�tre de configuration
		
	}
	
	/*
	 * M�thode de mise � jour (update) 
	 * Elle est lanc�e une fois que Init_jeu (la configuration de la partie) a fini
	 */
	protected void update() {
		try {
			if (this.le_mode_de_jeu == MODE_DE_JEU.Cardline)  // Si on est dans le mode de jeu cardline, on charge le critere du pays 
				charger_critere_cardline(false);
			tri_des_joueurs();
			chargement_des_cartes(le_mode_de_jeu);
			distribution_des_cartes();
			this.deck_central_frise.ajouter_carte(this.deck_du_jeu.remove_carte_aleatoire());
		} catch(Exception e) {
			System.out.println("Impossible de charger le jeu ! --> " + e.getMessage() + e.toString()); // Si le chargement du jeu ne c'est pas bien pass�, on affiche un message d'erreur
		}
		this.init_frame.update();
	}
	
	/*
	 * Fen�tre (pop-up) de s�lection du crit�re du pays
	 */
	protected CRITERE_CARDLINE charger_critere_cardline(boolean update) {
		if (this.le_mode_de_jeu == MODE_DE_JEU.Timeline) { // On v�rifi que on est bien dans le mode de jeu CardLine
			JOptionPane.showMessageDialog(
					null,
					"Vous ne pouvez pas changer le mode de jeu avec une partie de jeu TimeLine.",
					"Impossible", 
					JOptionPane.ERROR_MESSAGE);
			return null;
		} else {
			CRITERE_CARDLINE result = (CRITERE_CARDLINE)JOptionPane.showInputDialog(
	        frame,
	        "Vous avez choisi le jeu CardLine !\n"
	        + "Merci de choisir un crit�re de la ville",
	        "Choix du crit�re de la ville",
	        JOptionPane.PLAIN_MESSAGE,
	        new ImageIcon("C:\\Users\\PC-FIX Alex\\OneDrive - Universite de Lorraine\\COURS\\L3 MIAGE\\JAVA\\Projet\\workspace2\\Projet 0.4\\src\\data\\icon\\windows-question.png"),
	        CRITERE_CARDLINE.values(),
	        Carte_Cardline.getValeur_active());
			if (result == null)
				result = Carte_Cardline.getValeur_active();
			Carte_Cardline.setValeur_active(result);
			this.save_critereCardline = result;
			this.deck_central_frise.sort();
			if (update)
				this.init_frame.update();
	        return result;
		}

	}
	
	/*
	 * M�thode qui tri les joueurs en fonction de leurs ages
	 */
	@SuppressWarnings("unchecked")
	private void tri_des_joueurs() {
		Collections.sort(this.liste_des_joueurs);
		
	}

	/*
	 * M�thode qui charge les cartes (en fonction du mode de jeu
	 */
	private void chargement_des_cartes(MODE_DE_JEU jeu) throws Exception {
		Chargeur_cartes chargeur;
		if (jeu == MODE_DE_JEU.Cardline) {
			chargeur = new Chargeur_cartes(true);
		} else
			chargeur = new Chargeur_cartes(false);
		this.deck_du_jeu = chargeur.getDeck_du_jeu();
	}
	
	/*
	 * M�thode qui distribu les cartes aux joueurs en fonction du nombre de joueurs
	 */
	private void distribution_des_cartes() throws Exception {
		int nb_joueurs = this.liste_des_joueurs.size();
		int nb_carte_a_distribuer = 0;
		switch(nb_joueurs) {
		case 2: case 3:
			//Il faut distribuer 6 cartes par joueur
			nb_carte_a_distribuer = 6;
			break;
		case 4: case 5:
			//Il faut distribuer 5 cartes par joueur
			nb_carte_a_distribuer = 5;
			break;
		case 6: case 7: case 8:
			//Il faut distribuer 4 cartes par joueur
			nb_carte_a_distribuer = 4;
			break;
		}
		for (int i=0;i<nb_joueurs;i++) {
			for (int j=0;j<nb_carte_a_distribuer;j++) {
				this.liste_des_joueurs.get(i).getDeck_joueur().ajouter_carte(this.deck_du_jeu.remove_carte_aleatoire());
			}
		}
		
		
	}
	

	/*
	 * R�alise une action pour le joueur actif
	 */
	protected void actionJoueur(int nb_place) {
		if (placement_carte_sur_frise_centrale(this.carte_selectionnee,nb_place)) { // V�rification du placement
			this.liste_des_joueurs.get(getId_joueur_actif()).getDeck_joueur().remove_carte(this.carte_selectionnee);
			JOptionPane.showMessageDialog(
					null,
					"Vous avez bien plac� votre carte ! \n Sa valeur �t� : " + this.carte_selectionnee.getValeur(),
					"Bravo!", 
					JOptionPane.INFORMATION_MESSAGE);
			verification_fin();
		} else {
			JOptionPane.showMessageDialog(
					null,
					"Vous avez mal plac� votre carte ! \n Sa valeur �t� : " + this.carte_selectionnee.getValeur() + "\n Vous devez piocher une carte ! \n Une carte a �t� ajout�e � votre deck.",
					"Perdu !", 
					JOptionPane.ERROR_MESSAGE);
			try {
				this.liste_des_joueurs.get(getId_joueur_actif()).getDeck_joueur().remove_carte(this.carte_selectionnee); //  on retire cette carte � l'utilisateur
				this.liste_des_joueurs.get(getId_joueur_actif()).getDeck_joueur().ajouter_carte(this.deck_du_jeu.remove_carte_aleatoire()); // On rajoute une carte � l'uitlisateur
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		this.carte_selectionnee = null;
		nextJoueur();
		this.init_frame.update();
		
	}
	
	/*
	 * V�rification du placement de la carte jou�e sur la frise centrale
	 */
	private boolean placement_carte_sur_frise_centrale(Carte carte_jouee, int position) {
		boolean carte_bien_positionnee = verification_carte_centrale(carte_jouee,position);
		if (carte_bien_positionnee) {
			this.deck_central_frise.ajouter_carte(carte_jouee, position); // OK, ajout de la carte sur la frise
		} else {
			this.deck_cartes_supprimee.ajouter_carte(carte_jouee);		// KO, on l'ajoute au deck de carte supprim�e (Inutile, permet juste d'avoir un deck des cartes supprim� (utile pour des am�liorations supl�mentaire)
		}
		return carte_bien_positionnee;
	}
	
	/*
	 * V�rifi le placement en fonction de la valeur et position
	 */
	private boolean verification_carte_centrale(Carte carte_jouee, int position) {
		boolean carte_bien_positionnee = false;
		if (position == 0) {
			carte_bien_positionnee = this.deck_central_frise.getCartes().get(0).getValeur() > carte_jouee.getValeur();
		} else if (this.deck_central_frise.getNb_Cartes() == position) {
			carte_bien_positionnee = this.deck_central_frise.getCartes().get(this.deck_central_frise.getNb_Cartes()-1).getValeur() < carte_jouee.getValeur();
		} else {
			carte_bien_positionnee = this.deck_central_frise.getCartes().get(position-1).getValeur() < carte_jouee.getValeur() 
									 && 
									 this.deck_central_frise.getCartes().get(position).getValeur() > carte_jouee.getValeur(); 
		}
		return carte_bien_positionnee;
	}
	
	
	/*
	 * V�rification si c'est la fin de partie
	 */
	private void verification_fin() {
		if (this.liste_des_joueurs.get(getId_joueur_actif()).getDeck_joueur().getNb_Cartes() == 0) { // SI c'est la fin
			JOptionPane.showMessageDialog(
					null,
					"Le joueur : "+ this.liste_des_joueurs.get(getId_joueur_actif()).getPseudo()+" a gagn� ! \n "
							+ "Fin de la partie !",
					"FIN de partie", 
					JOptionPane.INFORMATION_MESSAGE);
			this.init_frame.close_game(); // On ferme le jeu
			this.init_frame.menu_jeu(); // on raffiche le menu d'une nouvelle partie ou chargement d'une partie
		}
	}

	/*
	 * M�thode pour passer au joueur suivant
	 */
	private void nextJoueur() {
		if (this.id_joueur_actif == this.liste_des_joueurs.size()-1) {
			this.id_joueur_actif = 0;
		} else {
			this.id_joueur_actif++;
		}
		
	}



	/*
	 * M�thode qui charge le crit�re du pays en cas de chargement d'une partie cardline sauvgard�e.
	 */
	protected void update_reload_game() {
		Carte_Cardline.setValeur_active(this.save_critereCardline);
	}
	
	/*
	 * M�thode qui permet de savoir le nombre de carte d'un joueur (Pour afficher dans le menu)
	 */
	protected int nb_cartes_restantes_joueur(int i) {
		return this.liste_des_joueurs.get(i).getDeck_joueur().getNb_Cartes();
	}
	
	/*
	 * M�thode qui permet de r�cup�rer le texte de la prochaine action
	 */
	protected String getProchaineAction() {
		if (this.carte_selectionnee == null) 
			return "Vous devez s�lectionner une carte de votre main !";
		else
			return "Vous devez selectionner un emplacement sur la zone de jeu !";
	}

	// Set
	protected void setCarte_selectionnee(Carte carte_selectionnee) {
		this.carte_selectionnee = carte_selectionnee;
	}
	
	protected void setListe_des_joueurs(ArrayList<Joueur> liste_des_joueurs) {
		this.liste_des_joueurs = liste_des_joueurs;
	}
	
	protected void setLe_mode_de_jeu(MODE_DE_JEU le_mode_de_jeu) {
		this.le_mode_de_jeu = le_mode_de_jeu;
	}
	
	protected void setInit_frame(Init init_frame) {
		this.init_frame = init_frame;
	}
	
	//Get
	protected String getNom_joueur_actif() {
		return this.liste_des_joueurs.get(this.id_joueur_actif).getPseudo();
	}
	
	protected int getId_joueur_actif() {
		return this.id_joueur_actif;
	}
	
	protected Deck getDeck_joueur_actif() {
		return this.liste_des_joueurs.get(this.id_joueur_actif).getDeck_joueur();
	}
	
	protected boolean carte_selectionnee() {
		return this.carte_selectionnee != null;
	}
	
	protected Deck getDeck_central_frise() {
		return deck_central_frise;
	}

	protected Deck getDeck_cartes_supprimee() {
		return deck_cartes_supprimee;
	}

	protected ArrayList<Joueur> getListe_des_joueurs() {
		return liste_des_joueurs;
	}

	protected MODE_DE_JEU getLe_mode_de_jeu() {
		return this.le_mode_de_jeu;
	}
	
	protected Carte getCarte_selectionnee() {
		return carte_selectionnee;
	}

}
