package fr.projet.jeu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

public class button_zone_centrale implements ActionListener {
	
	Init init;
	int nb_place;
	
	public button_zone_centrale(Init init,int nb_place) {
		this.init = init;
		this.nb_place = nb_place;
	}


	public void actionPerformed(ActionEvent arg0) {
		if (this.init.getLe_jeu().carte_selectionnee()) {
			this.init.getLe_jeu().actionJoueur(this.nb_place);
		} else {
			JOptionPane.showMessageDialog(
					null,
					"Vous devez d'abord sélectionner une carte de votre main !",
					"Erreur", 
					JOptionPane.ERROR_MESSAGE);
		}
	}
}
