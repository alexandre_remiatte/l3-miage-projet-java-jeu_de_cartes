package fr.projet.jeu;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import fr.projet.elements.Carte;

public class MouseAdapter_image extends MouseAdapter {

	private Carte la_carte;
	private Init init;

	
	public MouseAdapter_image(Carte la_carte,Init init) {
		this.la_carte = la_carte;
		this.init = init;
	}

	@Override
    public void mouseClicked(MouseEvent e) {
		this.init.getLe_jeu().setCarte_selectionnee(this.la_carte);
		this.init.update();
		
    }
	
}
