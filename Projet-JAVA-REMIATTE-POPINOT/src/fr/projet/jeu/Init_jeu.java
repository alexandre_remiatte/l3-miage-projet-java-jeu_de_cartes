package fr.projet.jeu;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import fr.projet.elements.Joueur;
import fr.projet.enums.MODE_DE_JEU;

public class Init_jeu {

	private JFrame frame;
	private JPanel contentPane;
	
	
	private ArrayList<Joueur> liste_des_joueurs = new ArrayList<Joueur>();
	private ArrayList<String> lesNomsDesJoueurs = new ArrayList<String>();
	private ArrayList<String> lesAgesDesJoueurs = new ArrayList<String>();
	
	
	private int nb_joueurs_max = 8;
	private ArrayList<JTextField> list_JTextField_noms = new ArrayList<JTextField>();
	private ArrayList<JTextField> list_JTextField_ages = new ArrayList<JTextField>();


	/**
	 * Create the frame.
	 */
	public Init_jeu(Jeu_de_cartes le_jeu) {
		frame = new JFrame();
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.frame.setBounds(100, 100, 589, 627);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setBackground(new Color(255, 204, 102));
		this.frame.setContentPane(contentPane);
		contentPane.setLayout(null);
		/*
		 * TITRE
		 */
		JLabel lblTitre = new JLabel("Configuration de la partie");
		lblTitre.setBounds(10, 11, 548, 20);
		lblTitre.setFont(new Font("Stencil", Font.PLAIN, 19));
		lblTitre.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblTitre);
		
		/*
		 * Sous titre MODE DE JEU
		 */
		JLabel lblSousTitre = new JLabel("Vous voulez jouer � quel jeu ?");
		lblSousTitre.setHorizontalAlignment(SwingConstants.CENTER);
		lblSousTitre.setBounds(154, 42, 280, 14);
		contentPane.add(lblSousTitre);
		
		
	
		 
		
		@SuppressWarnings({ "rawtypes", "unchecked" })
		JComboBox comboBox = new JComboBox(MODE_DE_JEU.values());
		comboBox.setBounds(250, 82, 100, 20);
		contentPane.add(comboBox);
		
		/*
		 * Sous titre Joueur
		 */
		JLabel lblSousTitreJoueur = new JLabel("Chaque joueurs doit donner son pr\u00E9nom et son \u00E2ge !");
		lblSousTitreJoueur.setHorizontalAlignment(SwingConstants.CENTER);
		lblSousTitreJoueur.setBounds(120, 149, 320, 14);
		contentPane.add(lblSousTitreJoueur);
		
		for (int i=0; i<nb_joueurs_max;i++) {
			
			JLabel lblJoueurNi = new JLabel("Joueur n\u00B0"+(i+1)+" :");
			lblJoueurNi.setBounds(102, 200+(i*36), 70, 14);
			contentPane.add(lblJoueurNi);
			
			JTextField txtNomJoueurNi = new JTextField();
			txtNomJoueurNi.setBounds(172, 195+(i*36), 150, 25);
			contentPane.add(txtNomJoueurNi);
			txtNomJoueurNi.setColumns(10);
			this.list_JTextField_noms.add(txtNomJoueurNi);
			
			
			JLabel lblAgeNi = new JLabel("\u00E2ge :");
			lblAgeNi.setBounds(339, 200+(i*36), 30, 14);
			contentPane.add(lblAgeNi);
			
			JTextField txtAgeJoueurNi = new JTextField();
			txtAgeJoueurNi.setBounds(374, 195+(i*36), 50, 25);
			contentPane.add(txtAgeJoueurNi);
			txtAgeJoueurNi.setColumns(10);
			this.list_JTextField_ages.add(txtAgeJoueurNi);
		}
		
		
		JButton btnJouer = new JButton("Jouer !");
		btnJouer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				for (int i=0;i<nb_joueurs_max;i++) {
					if (!list_JTextField_noms.get(i).getText().equals("")) {
						lesNomsDesJoueurs.add(list_JTextField_noms.get(i).getText());
						if (list_JTextField_ages.get(i).getText().equals("")) 
							lesAgesDesJoueurs.add("-1");
						else
							lesAgesDesJoueurs.add(list_JTextField_ages.get(i).getText());
					}
				}
				
				if (!verif_info_utilisateurs()) {
					JOptionPane.showMessageDialog(null, "Les informations ne sont pas juste !\n Merci de v�rifier les pr�noms et les �ges. \n [INFO] L'�ge doit �tre entre 0 et 124. (124 ?? Et pourquoi pas !)\n [INFO] Les pr�noms ne peuvent pas �tre identiques. \n [INFO] Il doit avoir entre 2 et 8 joueurs", "Erreur", JOptionPane.ERROR_MESSAGE);
				} else {
					for (int i=0;i<lesNomsDesJoueurs.size();i++) {
						liste_des_joueurs.add(new Joueur(lesNomsDesJoueurs.get(i),Integer.parseInt(lesAgesDesJoueurs.get(i))));
					}
					le_jeu.setLe_mode_de_jeu((MODE_DE_JEU) comboBox.getSelectedItem());
					le_jeu.setListe_des_joueurs(liste_des_joueurs);
					le_jeu.update();
					Init_jeu.this.frame.dispose();
				}
				
				while(lesAgesDesJoueurs.size() != 0) {
					lesAgesDesJoueurs.remove(0);
				}
				while(lesNomsDesJoueurs.size() != 0) {
					lesNomsDesJoueurs.remove(0);
				}
				
			}
		});
		btnJouer.setBounds(472, 550, 89, 23);
		contentPane.add(btnJouer);
		
		
		
		this.frame.setVisible(true);
		
	}
	
	
	boolean verif_info_utilisateurs() {
		boolean verif = true;
		
		// V�rif size()
		if(this.lesAgesDesJoueurs.size() != this.lesNomsDesJoueurs.size())
			verif = false;
		if (this.lesNomsDesJoueurs.size() < 2 || this.lesNomsDesJoueurs.size() > 8) 
			verif = false;
		for (int i=0;i<this.lesAgesDesJoueurs.size();i++) {
			if (Integer.parseInt(this.lesAgesDesJoueurs.get(i)) < 0 || Integer.parseInt(this.lesAgesDesJoueurs.get(i)) > 124)
				verif = false;
		}
		//V�rif doublons
		Set<String> verif_double = new HashSet<String>();
		for (String each: lesNomsDesJoueurs) if (!verif_double.add(each)) verif= false;
		
		return verif;
	}
}
