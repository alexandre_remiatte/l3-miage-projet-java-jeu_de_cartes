package fr.projet.enums;

public enum CRITERE_CARDLINE {
	SUPERFICIE,
	POPULATION,
	PIB,
	POLLUTION
}
