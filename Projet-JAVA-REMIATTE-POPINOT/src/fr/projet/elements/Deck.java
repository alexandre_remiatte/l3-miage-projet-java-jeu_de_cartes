package fr.projet.elements;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

@SuppressWarnings("serial")
public class Deck implements Serializable {
	
	private ArrayList<Carte> Cartes;

	public Deck() {
		this.Cartes = new ArrayList<Carte>();
	}
	
	public ArrayList<Carte> getCartes() {
		return Cartes;
	}
	
	public int getNb_Cartes() {
		return this.Cartes.size();
	}

	public void setCartes(ArrayList<Carte> cartes) {
		Cartes = cartes;
	}

	public int ajouter_carte(Carte une_carte) {
		this.Cartes.add(une_carte);
		return this.Cartes.size()-1;
	}
	
	public int  ajouter_carte(Carte carte_jouee, int position) {
		this.Cartes.add(position, carte_jouee);
		return this.Cartes.size()-1;
	}

	@Override
	public String toString() {
		return "Deck NB=" + this.Cartes.size() + "\n \t" + this.Cartes;
	}
	
	public Carte remove_carte(int id_carte) throws Exception {
		if (this.Cartes.size() == 0) {
			throw new Exception("Impossible de supprimer une carte ! Il n'y a plus de cartes dans le deck.");
		}
		return this.Cartes.remove(id_carte);
	}
	
	public boolean remove_carte(Carte carte_selectionnee) {
		if (this.Cartes.size() == 0) {
			System.out.println("Impossible de supprimer une carte ! Il n'y a plus de cartes dans le deck.");
		}
		return this.Cartes.remove(carte_selectionnee);
	}
	
	public Carte remove_carte_aleatoire() throws Exception {
		int nb_aleatoire = (int) (Math.random() * ( (this.Cartes.size()-1) - 0 ));
		return this.remove_carte(nb_aleatoire);
	}

	@SuppressWarnings("unchecked")
	public void sort() {
		Collections.sort(this.Cartes);
	}


}