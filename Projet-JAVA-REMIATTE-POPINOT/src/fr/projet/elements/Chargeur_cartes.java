package fr.projet.elements;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class Chargeur_cartes {

	
	final String chemin_fichier_Timeline = ".\\DATA\\timeline\\timeline.csv";
	final String chemin_images_Timeline = ".\\DATA\\timeline\\cards\\";
	
	final String chemin_fichier_Cardline = ".\\DATA\\cardline\\cardline.csv";
	final String chemin_images_Cardline = ".\\DATA\\cardline\\cards\\";
	
	Deck deck_du_jeu;
	
	public Deck getDeck_du_jeu() {
		return deck_du_jeu;
	}


	public Chargeur_cartes(boolean cardline) throws Exception { // par d�faut (false, c'est le jeu timeline qui est charg�)
		if (cardline) {
			this.deck_du_jeu = charger_fichier_Cardline();
		} else {
			this.deck_du_jeu = charger_fichier_timeline();
		}
	}
	
	
	public Deck charger_fichier_timeline() throws Exception {
		Deck tmp_deck = new Deck();
		try {
			@SuppressWarnings("resource")
			BufferedReader fichier_source = new BufferedReader(new FileReader(chemin_fichier_Timeline));
			String chaine;
			int i = 1;

			while ((chaine = fichier_source.readLine()) != null) {
				if (i > 1) {
					String[] tabChaine = chaine.split(";");
					if (tabChaine.length != 3) {
						throw new Exception("Le fichier de donn�es pour le jeu Timeline n'est correct ! Merci de v�rifier sont contenu. INFO[ligne : "+i+"]");
					} else {
						tmp_deck.ajouter_carte(new Carte_Timeline(tabChaine[0],Integer.parseInt(tabChaine[1]),chemin_images_Timeline+tabChaine[2]));
					}
				}
				i++;
			}
			fichier_source.close();
		} catch (FileNotFoundException e) {
			System.out.println("Le fichier est introuvable !" + e.getMessage());
		}
		return tmp_deck;
		
	}
	
	
	public Deck charger_fichier_Cardline() {
		Deck tmp_deck = new Deck();
		try {
			@SuppressWarnings("resource")
			BufferedReader fichier_source = new BufferedReader(new FileReader(chemin_fichier_Cardline));
			String chaine;
			int i = 1;

			while ((chaine = fichier_source.readLine()) != null) {
				if (i > 6) {
					String[] tabChaine = chaine.split(";");
					if (tabChaine.length != 6) {
						throw new Exception("Le fichier de donn�es pour le jeu Cardline n'est correct ! Merci de v�rifier sont contenu. INFO[ligne : "+i+"]");
					} else {
						tmp_deck.ajouter_carte(new Carte_Cardline(
								tabChaine[0],
								Double.parseDouble(tabChaine[1].replaceAll(",",".")),
								Integer.parseInt(tabChaine[2]),
								Integer.parseInt(tabChaine[3]),
								Double.parseDouble(tabChaine[4].replaceAll(",",".")),
								chemin_images_Cardline+tabChaine[5]));
					}
				}
				i++;
			}
			
			fichier_source.close();
		} catch (FileNotFoundException e) {
			System.out.println("Le fichier est introuvable !" + e.getMessage());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tmp_deck;
	}
}
