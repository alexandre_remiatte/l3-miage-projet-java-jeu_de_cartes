package fr.projet.elements;

import java.io.Serializable;
import java.util.Comparator;

@SuppressWarnings({ "serial", "rawtypes" })
public class Joueur implements Comparable, Serializable {
	
	String pseudo;
	int age;
	Deck deck_joueur;
	
	
	public Joueur(String pseudo, int age) {
		this.deck_joueur = new Deck();
		this.age = age;
		this.pseudo = pseudo;
	}


	public String getPseudo() {
		return pseudo;
	}


	public Deck getDeck_joueur() {
		return deck_joueur;
	}


	public void setDeck_joueur(Deck deck_joueur) {
		this.deck_joueur = deck_joueur;
	}


	@Override
	public String toString() {
		return "Joueur [pseudo=" + pseudo + ", age=" + age + ", deck_joueur=" + deck_joueur + "]";
	}



	public int getAge() {
		return age;
	}


	@Override
	public int compareTo(Object arg0) {
		if (this.getAge() == ((Joueur) arg0).getAge())
			return 0;
		else if (this.getAge() > ((Joueur) arg0).getAge())
			return 1;
		else
			return -1;
	}
	
	
}
