package fr.projet.elements;

import fr.projet.enums.CRITERE_CARDLINE;

@SuppressWarnings("serial")
public class Carte_Cardline extends Carte {
	

	static CRITERE_CARDLINE valeur_active;
	
	double superficie;
	int population;
	int pib; 
	double pollution;
	
	
	final String extension_image = ".jpg";
	final String nom_solution_image = "_reponse";
	
	public Carte_Cardline(String ville, double superficie, int population, int pib, double pollution, String chemin_image) {
		super(ville,chemin_image);
		this.superficie = superficie;
		this.population = population;
		this.pib = pib;
		this.pollution = pollution;
	}
	
	public static CRITERE_CARDLINE getValeur_active() {
		return valeur_active;
	}
	
	public static void setValeur_active(CRITERE_CARDLINE valeur_active) {
		Carte_Cardline.valeur_active = valeur_active;
	}
	
	@Override
	public String toString() {
		return "[" + this.nom + ", " + this.getValeur() +"]";
	}
	
	public double getValeur() {
		double valeur = 0;
		switch(Carte_Cardline.valeur_active) {
			case SUPERFICIE:
				valeur = this.superficie;
				break;
			case POPULATION:
				valeur = this.population;
				break;
			case PIB:
				valeur = this.pib;
				break;
			case POLLUTION:
				valeur = this.pollution;
				break;
		}
		return valeur;
	}
	
	
	public String getChemin_image() {
		return this.chemin_image + this.extension_image;
	}
	
	public String getChemin_image_reponse() {
		return this.chemin_image + this.nom_solution_image + this.extension_image;
	}
	
	
}
