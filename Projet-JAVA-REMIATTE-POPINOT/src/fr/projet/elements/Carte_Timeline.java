package fr.projet.elements;

@SuppressWarnings("serial")
public class Carte_Timeline extends Carte {
	
	int date;
	
	final String extension_image = ".jpg";
	final String nom_solution_image = "_date";
	
	
	public Carte_Timeline(String invention, int date, String chemin_image) {
		super(invention,chemin_image);
		this.date = date;
	}
	
	@Override
	public String toString() {
		return "[" + this.nom + ", " + this.getValeur() + "]";
	}
	
	public double getValeur() {
		return this.date;
	}
	
	public String getChemin_image() {
		return this.chemin_image + this.extension_image;
	}
	
	public String getChemin_image_reponse() {
		return this.chemin_image + this.nom_solution_image + this.extension_image;
	}
	
	
}