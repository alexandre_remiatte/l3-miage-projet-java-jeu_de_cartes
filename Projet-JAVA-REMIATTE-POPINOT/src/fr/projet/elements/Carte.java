package fr.projet.elements;

import java.io.Serializable;

@SuppressWarnings({ "serial", "rawtypes" })
public abstract class Carte implements Comparable, Serializable {
	
	protected String nom;
	protected String chemin_image;
	
	
	public Carte(String nom,String chemin_image) {
		this.nom = nom;
		this.chemin_image = chemin_image;
	}
	
	public abstract double getValeur();
	public abstract String getChemin_image();
	public abstract String getChemin_image_reponse();

	public String getNom() {
		return nom;
	}

	@Override
	public int compareTo(Object arg0) {
		if (this.getValeur() == ((Carte) arg0).getValeur())
			return 0;
		else if (this.getValeur() > ((Carte) arg0).getValeur())
			return 1;
		else
			return -1;
	}
	
}